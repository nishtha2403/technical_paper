# Caching

Caching is a technique to speed up data lookups.

Conventionally a data is fetched from it's source and this works fine for low request rates but with increasing request rates problems like low response time, reducing performance of other services or expensive to scale a database as call volume increases starts to show up to handle these problems we can add a cache and our services will be much improved.

## Different Caching Approaches

* Local Caches

* External Caches

* Inline Caches
  * Read Through
  * Write Through
  * Write Back

* Side Cache
  * Write Around

---

## **Local Cache**

* On-box cache is implemented in process memory.
* It is quick and easy to implement.
* Fairly low risk to integrate into an existing service.
* One way to implement on-box cache is by creating an in-memory table which is managed through application logic.

## **External Cache**

* External cache stores cached data in a separate fleet.
* Memcached or Redis can be used as an external cache.

## **Inline Cache**

* With inline cache implementation, cache sits inline with the database.

* When a data lookup is initiated by application the lookup is done only in cache.Data writing is also done only in cache.

> ### Benefits  
>
>* Inline Caching is a uniform API model for clients.
>* Inline Caching can be added or removed without any changes to client logic.

### **Read-Through**

In Read-Through when there is a cache miss, missing data is loaded from the database to the cache and cache returns the data to the application.

> Data modelling of *cache* and *database* needs to be same

### **Write-Through**

In Write-Through the data is written to the cache and cache immediatly writes data to the database.

> Read-Through Cache works best when paired with Write-Through Cache

> **DAX** (DynamoDB Accelerator) : Read-Through / Write-Through Cache
>
> * *DAX* sits inline with DynamoDB and the application
> * Read/write operation on DynamoDB can be done through *DAX*

### **Write-Back**

In Write-Back the data is written to the cache and after some delay cache writes data to the database.

> Primary benefit : Best for writing heavy workloads

> Main disadvantage : In case of *Cache failure* data will be permanently lost.

## **Side Cache**

>With side cache implementation, cache sits in side and application can converse to both cache and database.

> Data modelling in *Cache* can be different from *Database*

### **Write-Around**

In Write-Around data is written to the database directly and data is read from cache.

> *Write-Around Cache* works best when paired with *Read-Through Cache* for the situations where data is written once and read less frequently.

---

## **Simple Implementation of Caching**

```javascript
let cacheMemory = [];

//Calculating factorial of a number 
let  factorial = (num) => {

    //Looking for factorial of a no. in cache
    for(let obj of cacheMemory){
        if(num == obj.number){
            return obj.factorial;
        }
    }

    let factorial = 1;
     for(let i=1;i<=num;i++){
         factorial *= i;
     }

     //Storing the factorial in cacheMemory
     cacheMemory.push({number: num, factorial: factorial});

     return factorial;

}

console.time(`Time taken to find factorial of 1000000 without cache`);
factorial(1000000);
console.timeEnd(`Time taken to find factorial of 1000000 without cache`);

console.time(`Time taken to find factorial of 1000000 with cache`);
factorial(1000000);
console.timeEnd(`Time taken to find factorial of 1000000 with cache`);

```

```
Time taken to find factorial of 1000000 without cache: 5.666ms
Time taken to find factorial of 1000000 with cache: 0.024ms
```

## **Benefits of Caching**

* Caches reduces response time
* Decrease load on database
* Dependency size is reduced and thus database size can be scaled down
* Cost Saving

## **Need of choosing the right caching**

There are several caching strategies available and choosing the right one is very necessary. The right caching technique is the key to improved performance of the application.

Caching strategy depends on *data* and  *Data access pattern*.

## Cache Expiration

Cache implementation details which should be considered are *cache size*, *expiration policy*, and *eviction policy*.

Expiration policy determines how *long* to retain an *item* in the *cache*.

## References

[Caching Strategies and How to Choose the Right One ](https://codeahoy.com/2017/08/11/caching-strategies-and-how-to-choose-the-right-one/)

[Amazon Builder's library: Caching challenges and strategies](https://aws.amazon.com/builders-library/caching-challenges-and-strategies/#:~:text=Another%20decision%20we%20need%20to,implementation%20detail%20of%20that%20API)